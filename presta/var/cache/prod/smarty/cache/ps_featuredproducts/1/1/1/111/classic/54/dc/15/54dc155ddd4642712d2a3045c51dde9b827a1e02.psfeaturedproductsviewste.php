<?php
/* Smarty version 3.1.39, created on 2021-06-05 21:30:47
  from 'module:psfeaturedproductsviewste' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.39',
  'unifunc' => 'content_60bb8a970d8e88_14068023',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fa6cc378d2942c8857b89d6bca728048c0caeedd' => 
    array (
      0 => 'module:psfeaturedproductsviewste',
      1 => 1619601862,
      2 => 'module',
    ),
    '86a4d354f725902b1405db749b93834da03807f6' => 
    array (
      0 => '/var/www/html/themes/classic/templates/catalog/_partials/productlist.tpl',
      1 => 1619601862,
      2 => 'file',
    ),
    '4e48f9081812442e5797c9033049dad3e79d82e7' => 
    array (
      0 => '/var/www/html/themes/classic/templates/catalog/_partials/miniatures/product.tpl',
      1 => 1619601862,
      2 => 'file',
    ),
    '0724df70e9113f9ffcf0299fe2d091b4d46089e2' => 
    array (
      0 => '/var/www/html/themes/classic/templates/catalog/_partials/product-flags.tpl',
      1 => 1619601862,
      2 => 'file',
    ),
    'e64c699d70ea897ec9ed5b992b73872ad92542b6' => 
    array (
      0 => '/var/www/html/themes/classic/templates/catalog/_partials/variant-links.tpl',
      1 => 1619601862,
      2 => 'file',
    ),
  ),
  'cache_lifetime' => 31536000,
),true)) {
function content_60bb8a970d8e88_14068023 (Smarty_Internal_Template $_smarty_tpl) {
?><section class="featured-products clearfix">
  <h2 class="h2 products-section-title text-uppercase">
    Popular Products
  </h2>
  <div class="products row" itemscope itemtype="http://schema.org/ItemList">
            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="0" />  <article class="product-miniature js-product-miniature" data-id-product="1" data-id-product-attribute="1" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://sihawe.com/index.php?id_product=1&amp;id_product_attribute=1&amp;rewrite=hummingbird-printed-t-shirt&amp;controller=product&amp;id_lang=1#/1-size-s/8-color-white" class="thumbnail product-thumbnail">
            <img
              src="http://sihawe.com/img/p/1/1-home_default.jpg"
              alt="Hummingbird printed t-shirt"
              data-full-size-image-url="http://sihawe.com/img/p/1/1-large_default.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://sihawe.com/index.php?id_product=1&amp;id_product_attribute=1&amp;rewrite=hummingbird-printed-t-shirt&amp;controller=product&amp;id_lang=1#/1-size-s/8-color-white" itemprop="url" content="http://sihawe.com/index.php?id_product=1&amp;id_product_attribute=1&amp;rewrite=hummingbird-printed-t-shirt&amp;controller=product&amp;id_lang=1#/1-size-s/8-color-white">Hummingbird printed t-shirt</a></h3>
                  

        
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price" aria-label="Regular price">Rp26.29</span>
                                  <span class="discount-percentage discount-product">-20%</span>
                              
              

              <span class="price" aria-label="Price">Rp21.03</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="IDR" />
                <meta itemprop="price" content="21.03" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="1" data-url="http://sihawe.com/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag discount">-20%</li>
                    <li class="product-flag new">New</li>
            </ul>


      <div class="highlighted-informations hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Quick view
          </a>
        

        
                      <div class="variant-links">
      <a href="http://sihawe.com/index.php?id_product=1&amp;id_product_attribute=3&amp;rewrite=hummingbird-printed-t-shirt&amp;controller=product&amp;id_lang=1#/2-size-m/8-color-white"
       class="color"
       title="White"
       aria-label="White"
       style="background-color: #ffffff"     ></a>
      <a href="http://sihawe.com/index.php?id_product=1&amp;id_product_attribute=2&amp;rewrite=hummingbird-printed-t-shirt&amp;controller=product&amp;id_lang=1#/1-size-s/11-color-black"
       class="color"
       title="Black"
       aria-label="Black"
       style="background-color: #434A54"     ></a>
    <span class="js-count count"></span>
</div>
                  
      </div>
    </div>
  </article>
</div>

            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="1" />  <article class="product-miniature js-product-miniature" data-id-product="2" data-id-product-attribute="9" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://sihawe.com/index.php?id_product=2&amp;id_product_attribute=9&amp;rewrite=brown-bear-printed-sweater&amp;controller=product&amp;id_lang=1#/1-size-s" class="thumbnail product-thumbnail">
            <img
              src="http://sihawe.com/img/p/2/1/21-home_default.jpg"
              alt="Brown bear printed sweater"
              data-full-size-image-url="http://sihawe.com/img/p/2/1/21-large_default.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://sihawe.com/index.php?id_product=2&amp;id_product_attribute=9&amp;rewrite=brown-bear-printed-sweater&amp;controller=product&amp;id_lang=1#/1-size-s" itemprop="url" content="http://sihawe.com/index.php?id_product=2&amp;id_product_attribute=9&amp;rewrite=brown-bear-printed-sweater&amp;controller=product&amp;id_lang=1#/1-size-s">Hummingbird printed sweater</a></h3>
                  

        
                      <div class="product-price-and-shipping">
                              

                <span class="regular-price" aria-label="Regular price">Rp39.49</span>
                                  <span class="discount-percentage discount-product">-20%</span>
                              
              

              <span class="price" aria-label="Price">Rp31.59</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="IDR" />
                <meta itemprop="price" content="31.59" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="2" data-url="http://sihawe.com/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag discount">-20%</li>
                    <li class="product-flag new">New</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Quick view
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="2" />  <article class="product-miniature js-product-miniature" data-id-product="3" data-id-product-attribute="13" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://sihawe.com/index.php?id_product=3&amp;id_product_attribute=13&amp;rewrite=the-best-is-yet-to-come-framed-poster&amp;controller=product&amp;id_lang=1#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
            <img
              src="http://sihawe.com/img/p/3/3-home_default.jpg"
              alt="The best is yet to come&#039; Framed poster"
              data-full-size-image-url="http://sihawe.com/img/p/3/3-large_default.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://sihawe.com/index.php?id_product=3&amp;id_product_attribute=13&amp;rewrite=the-best-is-yet-to-come-framed-poster&amp;controller=product&amp;id_lang=1#/19-dimension-40x60cm" itemprop="url" content="http://sihawe.com/index.php?id_product=3&amp;id_product_attribute=13&amp;rewrite=the-best-is-yet-to-come-framed-poster&amp;controller=product&amp;id_lang=1#/19-dimension-40x60cm">The best is yet to come&#039;...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">Rp31.90</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="IDR" />
                <meta itemprop="price" content="31.9" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="3" data-url="http://sihawe.com/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag new">New</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Quick view
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="3" />  <article class="product-miniature js-product-miniature" data-id-product="4" data-id-product-attribute="16" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://sihawe.com/index.php?id_product=4&amp;id_product_attribute=16&amp;rewrite=the-adventure-begins-framed-poster&amp;controller=product&amp;id_lang=1#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
            <img
              src="http://sihawe.com/img/p/4/4-home_default.jpg"
              alt="The adventure begins Framed poster"
              data-full-size-image-url="http://sihawe.com/img/p/4/4-large_default.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://sihawe.com/index.php?id_product=4&amp;id_product_attribute=16&amp;rewrite=the-adventure-begins-framed-poster&amp;controller=product&amp;id_lang=1#/19-dimension-40x60cm" itemprop="url" content="http://sihawe.com/index.php?id_product=4&amp;id_product_attribute=16&amp;rewrite=the-adventure-begins-framed-poster&amp;controller=product&amp;id_lang=1#/19-dimension-40x60cm">The adventure begins Framed...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">Rp31.90</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="IDR" />
                <meta itemprop="price" content="31.9" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="4" data-url="http://sihawe.com/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag new">New</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Quick view
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="4" />  <article class="product-miniature js-product-miniature" data-id-product="5" data-id-product-attribute="19" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://sihawe.com/index.php?id_product=5&amp;id_product_attribute=19&amp;rewrite=today-is-a-good-day-framed-poster&amp;controller=product&amp;id_lang=1#/19-dimension-40x60cm" class="thumbnail product-thumbnail">
            <img
              src="http://sihawe.com/img/p/5/5-home_default.jpg"
              alt="Today is a good day Framed poster"
              data-full-size-image-url="http://sihawe.com/img/p/5/5-large_default.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://sihawe.com/index.php?id_product=5&amp;id_product_attribute=19&amp;rewrite=today-is-a-good-day-framed-poster&amp;controller=product&amp;id_lang=1#/19-dimension-40x60cm" itemprop="url" content="http://sihawe.com/index.php?id_product=5&amp;id_product_attribute=19&amp;rewrite=today-is-a-good-day-framed-poster&amp;controller=product&amp;id_lang=1#/19-dimension-40x60cm">Today is a good day Framed...</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">Rp31.90</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="IDR" />
                <meta itemprop="price" content="31.9" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="5" data-url="http://sihawe.com/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag new">New</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Quick view
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="5" />  <article class="product-miniature js-product-miniature" data-id-product="6" data-id-product-attribute="0" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://sihawe.com/index.php?id_product=6&amp;rewrite=mug-the-best-is-yet-to-come&amp;controller=product&amp;id_lang=1" class="thumbnail product-thumbnail">
            <img
              src="http://sihawe.com/img/p/6/6-home_default.jpg"
              alt="Mug The best is yet to come"
              data-full-size-image-url="http://sihawe.com/img/p/6/6-large_default.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://sihawe.com/index.php?id_product=6&amp;rewrite=mug-the-best-is-yet-to-come&amp;controller=product&amp;id_lang=1" itemprop="url" content="http://sihawe.com/index.php?id_product=6&amp;rewrite=mug-the-best-is-yet-to-come&amp;controller=product&amp;id_lang=1">Mug The best is yet to come</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">Rp13.09</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="IDR" />
                <meta itemprop="price" content="13.09" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="6" data-url="http://sihawe.com/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag new">New</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Quick view
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="6" />  <article class="product-miniature js-product-miniature" data-id-product="7" data-id-product-attribute="0" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://sihawe.com/index.php?id_product=7&amp;rewrite=mug-the-adventure-begins&amp;controller=product&amp;id_lang=1" class="thumbnail product-thumbnail">
            <img
              src="http://sihawe.com/img/p/7/7-home_default.jpg"
              alt="Mug The adventure begins"
              data-full-size-image-url="http://sihawe.com/img/p/7/7-large_default.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://sihawe.com/index.php?id_product=7&amp;rewrite=mug-the-adventure-begins&amp;controller=product&amp;id_lang=1" itemprop="url" content="http://sihawe.com/index.php?id_product=7&amp;rewrite=mug-the-adventure-begins&amp;controller=product&amp;id_lang=1">Mug The adventure begins</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">Rp13.09</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="IDR" />
                <meta itemprop="price" content="13.09" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="7" data-url="http://sihawe.com/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag new">New</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Quick view
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

            
<div itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem" class="product">
  <meta itemprop="position" content="7" />  <article class="product-miniature js-product-miniature" data-id-product="8" data-id-product-attribute="0" itemprop="item" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      
                  <a href="http://sihawe.com/index.php?id_product=8&amp;rewrite=mug-today-is-a-good-day&amp;controller=product&amp;id_lang=1" class="thumbnail product-thumbnail">
            <img
              src="http://sihawe.com/img/p/8/8-home_default.jpg"
              alt="Mug Today is a good day"
              data-full-size-image-url="http://sihawe.com/img/p/8/8-large_default.jpg"
              />
          </a>
              

      <div class="product-description">
        
                      <h3 class="h3 product-title" itemprop="name"><a href="http://sihawe.com/index.php?id_product=8&amp;rewrite=mug-today-is-a-good-day&amp;controller=product&amp;id_lang=1" itemprop="url" content="http://sihawe.com/index.php?id_product=8&amp;rewrite=mug-today-is-a-good-day&amp;controller=product&amp;id_lang=1">Mug Today is a good day</a></h3>
                  

        
                      <div class="product-price-and-shipping">
              
              

              <span class="price" aria-label="Price">Rp13.09</span>
              <div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="invisible">
                <meta itemprop="priceCurrency" content="IDR" />
                <meta itemprop="price" content="13.09" />
              </div>

              

              
            </div>
                  

        
          
<div class="product-list-reviews" data-id="8" data-url="http://sihawe.com/index.php?fc=module&module=productcomments&controller=CommentGrade&id_lang=1">
  <div class="grade-stars small-stars"></div>
  <div class="comments-nb"></div>
</div>


        
      </div>

      
    <ul class="product-flags">
                    <li class="product-flag new">New</li>
            </ul>


      <div class="highlighted-informations no-variants hidden-sm-down">
        
          <a class="quick-view" href="#" data-link-action="quickview">
            <i class="material-icons search">&#xE8B6;</i> Quick view
          </a>
        

        
                  
      </div>
    </div>
  </article>
</div>

    </div>  <a class="all-product-link float-xs-left float-md-right h4" href="http://sihawe.com/index.php?id_category=2&amp;controller=category&amp;id_lang=1">
    All products<i class="material-icons">&#xE315;</i>
  </a>
</section>
<?php }
}
